-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 04-11-2019 a las 22:45:30
-- Versión del servidor: 5.7.27-0ubuntu0.18.04.1
-- Versión de PHP: 7.2.24-0ubuntu0.18.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `quizzer`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `choices`
--

CREATE TABLE `choices` (
  `id` int(11) NOT NULL,
  `question_number` int(11) NOT NULL,
  `is_correct` tinyint(1) NOT NULL DEFAULT '0',
  `text` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `choices`
--

INSERT INTO `choices` (`id`, `question_number`, `is_correct`, `text`) VALUES
(1, 1, 1, ' PHP: Hypertext Preprocessor'),
(2, 1, 0, ' Private Home Page'),
(3, 1, 0, ' Personal Hypertext Processor'),
(4, 2, 1, ' echo \"Hello World\";'),
(5, 2, 0, ' Document.Write(\"Hello World\");'),
(6, 2, 0, ' \"Hello World\";'),
(7, 3, 0, ' <?php...<?>'),
(8, 3, 0, ' <script>...</script>'),
(9, 3, 1, ' <?php...?>'),
(10, 3, 0, ' <?php>...</?>'),
(11, 4, 0, '&'),
(12, 4, 1, '$'),
(13, 4, 0, '!'),
(14, 5, 0, ' .'),
(15, 5, 1, ' ;'),
(16, 5, 0, ' New line'),
(17, 5, 0, ' </php>'),
(18, 6, 0, ' VBScript'),
(19, 6, 0, ' JavaScript'),
(20, 6, 1, ' Pearl and C'),
(21, 7, 0, 'new_function myFunction()'),
(22, 7, 0, 'create myFunction()'),
(23, 7, 1, 'function myFunction()'),
(24, 8, 0, ' $_POST [ ];'),
(25, 8, 0, ' Request.QueryString;'),
(26, 8, 1, ' $_GET [ ];'),
(27, 8, 0, ' Request.Form'),
(28, 9, 0, ' count++;'),
(29, 9, 0, ' $count=+1;'),
(30, 9, 1, ' $count++;'),
(31, 9, 0, ' ++count'),
(32, 10, 0, ' <!-- ... ->'),
(33, 10, 0, ' * ... *'),
(34, 10, 0, ' <comment> ... </comment>'),
(35, 10, 1, ' /* ... */');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `questions`
--

CREATE TABLE `questions` (
  `question_number` int(11) NOT NULL,
  `text` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `questions`
--

INSERT INTO `questions` (`question_number`, `text`) VALUES
(1, 'PHP son las iniciales para:'),
(2, 'Como se escribe \"Hello World\" en PHP'),
(3, 'Los scripts PHP se delimitan con...'),
(4, 'Todas las variables en PHP comienzan con el simbolo...'),
(5, 'Cual es la forma correcta de terminar una sentencia PHP?'),
(6, 'La sintaxis PHP es mas similar a:'),
(7, 'Cual es la forma correcta de crear una funcion en PHP?'),
(8, 'Como se obtiene informacion de un formulario que se envia utilizando el metodo GET'),
(9, 'Cual es la forma correcta de incrementar en 1 a la variable $count'),
(10, ' Cual es la forma correcta de agregar un comentario en PHP?');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `choices`
--
ALTER TABLE `choices`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`question_number`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `choices`
--
ALTER TABLE `choices`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
